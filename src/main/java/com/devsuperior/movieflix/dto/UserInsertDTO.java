package com.devsuperior.movieflix.dto;

import com.devsuperior.movieflix.entities.User;

public class UserInsertDTO extends UserDTO {

    private String password;

    public UserInsertDTO(User user, String password) {
        super(user);
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
