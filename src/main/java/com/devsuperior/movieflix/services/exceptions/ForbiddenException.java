package com.devsuperior.movieflix.services.exceptions;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 2022/07/19
 */

public class ForbiddenException extends RuntimeException {

  public ForbiddenException(String message) {
	super(message);
  }

}
