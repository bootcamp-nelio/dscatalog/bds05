package com.devsuperior.movieflix.services.exceptions;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 2022/07/19
 */

public class UnauthorizedException extends RuntimeException {

  public UnauthorizedException(String message) {
	super(message);
  }

}
